(define-module (atuin packages game-development)
  #:use-module (guix packages)
  #:use-module ((guix licenses)
		#:prefix license:)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages gtk))

(define-public tic-80
  ;; TODO: Package TIC-80 with an additional output as a WASM module.
  (package
    (name "tic-80")
    (version "0.80.1344")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/nesbox/TIC-80")
                    (commit (string-append "v" version))
                    (recursive? #t)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0b1qm1m1wwss4bh3q8gx26kk4g8j4n9mzdd4lbyj7k37fx5v09rp"))))
    (build-system cmake-build-system)
    (arguments
     `(#:tests? #f                      ; No tests.
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'patch-source
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bin (string-append out "/bin"))
                    (share/applications (string-append out "/share/applications"))
                    (share/icons (string-append out "/share/icons")))
               (substitute* "CMakeLists.txt"
                 ;; Replace the destinations to be inline with Guix.
                 (("/usr/share/applications") bin)
                 (("/usr/share/icons") share/icons)
                 (("\\$\\{TIC80_DESKTOP_DIR}") share/applications))

               ;; The files inside of the folder should have write permissions
               ;; but it will be overwritten by the build process anyways so it
               ;; is faster to delete them instead.
               (delete-file-recursively "build/assets")
               (mkdir-p "build/assets"))
             #t)))))
    (inputs
     `(("alsa-lib" ,alsa-lib)
       ("curl" ,curl)
       ("freeglut" ,freeglut)
       ("glu" ,glu)
       ("gtk+" ,gtk+)
       ("mesa" ,mesa)))
    (native-inputs
     `(("pkg-config" ,pkg-config)))
    (home-page "https://tic80.com")
    (synopsis "Fantasy computer with built-in tools for game development")
    (description
     "A fantasy computer featuring built-in tools such as a sprite editor,
music tracker, and a text editor.  This package builds the free version of the
software which you can override the package and build the PRO version
yourself.")
    (license license:expat)))
