(define-module (atuin packages sphinx)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix build utils)
  #:use-module (guix build-system python)
  #:use-module (gnu packages python-xyz)
  #:use-module ((gnu packages sphinx)
                #:select
                ((python-sphinx-copybutton . guix:python-sphinx-copybutton)
                 python-sphinx)))

(define* (warehouse-tarball name version #:optional (extension ".tar.gz"))
  (string-append name "-" version extension))

(define* (warehouse-uri prefix name version #:optional (extension ".tar.gz"))
  (string-append "https://files.pythonhosted.org/packages/" prefix "/"
                 (warehouse-tarball name version extension)))

(define-public python-sphinx-opengraph
  (package
    (name "python-sphinx-opengraph")
    (version "0.5.1")
    (source
     (origin
       (method url-fetch)
       (uri (warehouse-uri
             "9d/86/d15957e0d2f3a4ab3ef0376365e1a9cff797dddcae71f3ceb35438f7c363"
             "sphinxext-opengraph" version))
       (sha256
        (base32 "198614lq5a3xz93f6vq6bgkfi8n3ibrc9awj3249lx0c9655lrvq"))))
    (build-system python-build-system)
    (propagated-inputs (list python-sphinx))
    (home-page "https://github.com/wpilibsuite/sphinxext-opengraph")
    (synopsis "Sphinx Extension to enable OGP support")
    (description "Sphinx Extension to enable OGP support")
    (license #f)))

;; TODO: How to use this:
;; #:modules ((guix build utils)
;;            (guix build python-build-system))
;;            ;; (guix utils))
;; #:imported-modules (,@%python-build-system-modules
;;                     (guix utils))
(define-public python-sphinx-inline-tabs
  (let* ((warehouse-name "sphinx_inline_tabs")
         (version "2022.1.2b11")
         (tgz-file (warehouse-tarball warehouse-name version))
         (tgz-dir (warehouse-tarball warehouse-name version "")))
    (package
      (name "python-sphinx-inline-tabs")
      (version version)
      (source
       (origin
         (method url-fetch)
         (uri
          (warehouse-uri
           "65/ed/120f0f62243aa3fbb6a22fb28940ffdfa783013700c82d445e687b86f0bb"
           warehouse-name version))
         (sha256
          (base32 "0wyx7wmq2kk97j0fw9hvcdazr241a4diin05gyqcq1gcf8ki9fdg"))))
      (build-system python-build-system)
      (arguments
       ;; the source does not have a setup.py it needs to be generated using
       ;; flit first
       `(#:phases (modify-phases %standard-phases
                    (add-before 'build 'build-setup
                      (lambda _
                        (invoke "flit" "build" "--setup-py")
                        (invoke "tar" "xvf" (string-append "dist/" ,tgz-file))
                        (chdir ,tgz-dir))))))
      (propagated-inputs (list python-sphinx python-flit))
      (inputs (list python-flit))
      (home-page "https://github.com/pradyunsg/sphinx-inline-tabs")
      (synopsis "Add inline tabbed content to your Sphinx documentation.")
      (description "Add inline tabbed content to your Sphinx documentation.")
      (license #f))))

(define-public python-sphinx-copybutton
  (package
    (inherit guix:python-sphinx-copybutton)
    (name "python-sphinx-copybutton")
    (version "0.4.0")
    (source
     (origin
       (inherit (package-source guix:python-sphinx-copybutton))
       (method url-fetch)
       (uri (pypi-uri "sphinx-copybutton" version))
       (sha256
        (base32
         "11lkd1xc15gq8fr3vkaw0x9c1gnmqif5gwws78y03mdghwxd3bld"))))))

(define-public python-sphinx-furo
  (let* ((warehouse-name "furo")
         (version "2022.1.2")
         (tgz-file (warehouse-tarball warehouse-name version))
         (tgz-dir (warehouse-tarball warehouse-name version "")))
    (package
      (name "python-sphinx-furo")
      (version "2022.1.2")
      (source
       (origin
         (method url-fetch)
         (uri (pypi-uri "furo" version))
         (sha256
          (base32 "0c7db9xxlcgg8hy5j3bgv3rfpv6li69sgfk9zvxkyhndrccg45xj"))))
      (build-system python-build-system)
      (propagated-inputs (list python-sphinx python-beautifulsoup4))
      (inputs (list python-flit))
      (arguments
       ;; the source does not have a setup.py it needs to be generated using
       ;; flit first
       `(#:phases (modify-phases %standard-phases
                    (add-before 'build 'build-setup
                      (lambda _
                        (invoke "flit" "build" "--setup-py")
                        (invoke "tar" "xvf" (string-append "dist/" ,tgz-file))
                        (chdir ,tgz-dir))))
         #:tests? #f))
      (home-page "")
      (synopsis "A clean customisable Sphinx documentation theme.")
      (description
       "This package provides a clean customisable Sphinx documentation theme.")
      (license #f))))
