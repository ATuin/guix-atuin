(define-module (atuin packages suckless)
  #:use-module (atuin packages)
  #:use-module (guix packages)
  #:use-module ((gnu packages suckless)
		        #:select (st)
		        #:prefix guix:))

(define (%st-patches)
  (search-package-patches guix:st "st-anysize"))

(define-public st
  (package
    (inherit guix:st)
    (name "st")
    (source
     (origin
       (inherit (package-source guix:st))
       (patches (%st-patches))))))
