(define-module (atuin packages python-xyz)
  #:use-module (guix download)
  #:use-module ((guix licenses)
		#:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix build-system python)
  #:use-module ((gnu packages python-xyz)
		#:select (python-invoke))
  #:use-module ((gnu packages check)
		#:select (python-pytest
			  python-mock))
  #:use-module ((gnu packages python-crypto)
		#:select (python-paramiko)))

(define-public python-fabric
  (package
    (name "python-fabric")
    (version "2.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
	     "https://github.com/fabric/fabric/archive/" version ".tar.gz"))
       (sha256
	(base32
	 "171q866fgd86wjjh4nanp2nf2g1pi46bamkpbrbxwwn3piwlqgsb"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #t))
    (propagated-inputs
     `(("python-invoke" ,python-invoke)
       ("python-paramiko" ,python-paramiko)))
    (native-inputs
     `(("python-mock" ,python-mock)
       ("python-pytest" ,python-pytest)))
    (home-page "http://fabfile.org")
    (synopsis "High level SSH command execution")
    (description "High level SSH command execution")
    (license license:bsd-3)))
