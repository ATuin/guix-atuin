(define-module (atuin packages wm)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module ((gnu packages wm)
                #:prefix guix:))

(define-public stumpwm+slynk+extensions
  (package
   (inherit guix:stumpwm+slynk)
   (name "stump-with-slynk-and-extensions")
   (arguments
     (substitute-keyword-arguments
      (package-arguments guix:stumpwm+slynk)
      ((#:phases phases)
       `(modify-phases ,phases
         (add-after 'build-program 'wrap-prorgam
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (program (string-append out "/bin/stumpwm"))
                    (sbcl-home (string-append (assoc-ref inputs "sbcl") "/lib/sbcl")))
               (wrap-program (string-append out "/bin/stumpwm")
                 `("SBCL_HOME" = (,sbcl-home)))
               #t)))))))))
