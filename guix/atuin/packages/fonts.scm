(define-module (atuin packages fonts)
  #:use-module (guix build-system font)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:))

(define-public font-adi1090x-rofi
  (package
    (name "font-adi1090x-rofi")
    (version "1.6.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/adi1090x/rofi")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1yb4iz6x9v3hla2k8721lgvm6kajx0lfrwq5xj3hjhhxbbzdybm7"))))
    (build-system font-build-system)
    (arguments
     '(#:phases (modify-phases %standard-phases
                  (add-before 'patch-source-shebangs 'chdir-fonts
                    (lambda* (#:key outputs #:allow-other-keys)
                      (chdir "fonts"))))))
    (home-page "https://github.com/adi1090x/rofi")
    (synopsis
     "Fonts used in the rofi themes created by adi1090x")
    (description
     "Fonts used in the rofi themes created by adi1090x")
    (license license:gpl3)))

(define-public font-et-book
  (package
    (name "font-et-book")
    (version "24d3a3b")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/edwardtufte/et-book")
             (commit "24d3a3bbfc880095d3df2b9e9d60d05819138e89")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1bhpk1fbp8jdhzc5j8y5v3bpnzy2sz3dpgjgzclb0dnm51hqqrpn"))))
    (build-system font-build-system)
    (home-page "https://github.com/edwardtufte/et-book/")
    (synopsis
     "A webfont version of the typeface used in Edward Tufte’s books.")
    (description
     "A webfont version of the typeface used in Edward Tufte’s books. ET Book
was designed by Dmitry Krasny, Bonnie Scranton, and Edward Tufte. It was
converted from the original suit files by Adam Schwartz (@adamschwartz).")
     (license (license:non-copyleft
               (string-append "https://raw.githubusercontent.com/"
                              "edwardtufte/et-book/gh-pages/LICENSE")))))
