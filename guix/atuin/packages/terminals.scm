(define-module (atuin packages terminals)
  #:use-module ((atuin packages sphinx)
                #:prefix atuin:)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (gnu packages rsync)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages sphinx)
  #:use-module ((gnu packages terminals)
                #:select (kitty)
                #:prefix guix:))

(define kitty-version "0.24.1")
(define-public kitty
  (package
    (inherit guix:kitty)
    (version kitty-version)
    (name "kitty")
    (source
     (origin
       (inherit (package-source guix:kitty))
       (uri (git-reference
             (url "https://github.com/kovidgoyal/kitty")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "15vg0lzgil2jg39j39y58fld4yffbqkal9n5jb8xbh62pswk5yaq"))
       ;; (modules '((guix build utils)))
       (snippet
        '(begin
           ;; Don't build html documentation. It requires npm and lots of
           ;; dependencies. Not worth it.
           (substitute* "Makefile"
             (("docs: man html")
              "docs: man"))
           (substitute* "setup.py"
             (("copy_html_docs\\(ddir\\)")
              ""))
           #t))))
    (native-inputs
     `(("libdbus" ,dbus)
       ("libgl1-mesa" ,mesa)
       ("libxcursor" ,libxcursor)
       ("libxi" ,libxi)
       ("libxinerama" ,libxinerama)
       ("libxkbcommon" ,libxkbcommon)
       ("libxrandr" ,libxrandr)
       ("ncurses" ,ncurses) ;; for tic command
       ("pkg-config" ,pkg-config)
       ("python-sphinx" ,python-sphinx)
       ("python-sphinx-copybutton" ,atuin:python-sphinx-copybutton)
       ("python-sphinx-opengraph" ,atuin:python-sphinx-opengraph)
       ("python-sphinx-inline-tabs" ,atuin:python-sphinx-inline-tabs)
       ("wayland-protocols" ,wayland-protocols)
       ("librsync",librsync)))))
