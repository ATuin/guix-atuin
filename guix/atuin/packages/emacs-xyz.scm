(define-module (atuin packages emacs-xyz)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (gnu packages)
  #:use-module ((gnu packages emacs-xyz)
        :select (emacs-dumb-jump)
        :prefix guix:))

(define-public emacs-dumb-jump
  (let ((commit "8bc195000e17ce6c72755a8fb55ca0fcd36add76")
    (revision "1")
    (version "0.5.3")
    (name (package-name guix:emacs-dumb-jump)))
    (package
      (inherit guix:emacs-dumb-jump)
      (version (git-version version revision commit))
      (source
       (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/AitorATuin/dumb-jump")
           (commit commit)))
     (file-name (git-file-name name version))
     (sha256 (base32 "0dc31yy4r41nwwv57dzbd6zgwddizqh4q4qagpqx645l509s7k7i")))))))
