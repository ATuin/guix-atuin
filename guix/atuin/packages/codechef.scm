(define-module (atuin packages codechef)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module ((gnu packages chicken)
		#:select (chicken)
		#:prefix guix:)
  #:use-module ((gnu packages lisp)
		#:select (sbcl)
		#:prefix guix:))

(define-public chicken-codechef
  (package
    (inherit guix:chicken)
    (name "chicken-codechef")
    (version "4.11.0")
    (source
     (origin
       (inherit (package-source guix:chicken))
       (uri (string-append "https://code.call-cc.org/releases/"
			   version "/chicken-" version ".tar.gz"))
       (sha256
	(base32
	 "12ddyiikqknpr8h6llsxbg2fz75xnayvcnsvr1cwv8xnjn7jpp73"))))
    (description
     "CHICKEN is a compiler for the Scheme programming language.  CHICKEN
produces portable and efficient C, supports almost all of the R5RS Scheme
language standard, and includes many enhancements and extensions.
This is the version used in codechef programming contest site.")))

(define-public sbcl-codechef
  (package
    (inherit guix:sbcl)
    (version "1.3.13")
    (name "sbcl-codechef")
    (source
     (origin
       (inherit (package-source guix:sbcl))
       (uri (string-append "mirror://sourceforge/sbcl/sbcl/" version "/sbcl-"
			   version "-source.tar.bz2"))
       (sha256
	(base32
	 "1k3nij1pchkard02p51mbbsn4rrj116v1apjjpd3f9h2m7j3asac"))))
    (description "Steel Bank Common Lisp (SBCL) is a high performance Common
Lisp compiler.  In addition to the compiler and runtime system for ANSI Common
Lisp, it provides an interactive environment including a debugger, a
statistical profiler, a code coverage tool, and many other extensions.
This is the version used in codechef programming contest site.")))
