(define-module (atuin packages package-management)
  #:use-module (atuin packages)
  #:use-module (guix packages)
  #:use-module ((gnu packages package-management)
		        #:select (stow)
		        #:prefix guix:))

(define (%stow-patches)
  (search-package-patches guix:stow "stow-56727"))

(define-public stow
  (package
   (inherit guix:stow)
   (name "stow")
   (source
    (origin
     (inherit (package-source guix:stow))
     (patches (%stow-patches))))))
