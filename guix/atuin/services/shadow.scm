(define-module (atuin services shadow)
  #:use-module (srfi srfi-1)
  #:use-module (guix records)
  #:use-module (gnu services)
  #:use-module (gnu system shadow)
  #:use-module (guix gexp)
  #:export (account->subid-account
            subid-service-type
            subid-configuration))

;; etc_subuid and etc_subgid service
(define-record-type* <subid-entry> subid-entry make-subid-entry
  subid-entry?
  (name subid-entry-name)
  (init-id subid-entry-init-id)
  (last-id subid-entry-last-id))

(define-record-type* <subid-account> subid-account make-subid-account
  subid-account?
  (subuid subid-account-subuid)
  (subgid subid-account-subgid))

(define-record-type* <subuid-configuration>
  subid-configuration make-subid-configuration subid-configuration?
  (accounts subid-configuration-accounts (default '())))

(define* (account->subid-account user-account user-group
                                 #:optional (last-uid 65536)
                                 (last-gid 65536))
  (subid-account (subuid (subid-entry (name (user-account-name user-account))
                                      (init-id (or
                                                (user-account-uid user-account)
                                                1000))
                                      (last-id last-uid)))
                 (subgid (subid-entry (name (user-group-name user-group))
                                      (init-id (user-group-id user-group))
                                      (last-id last-gid)))))
(define (/etc-subid-entries config)
  (define (subid-contents selector)
    (fold (lambda (entry contents)
            (string-append (format #f "~a:~a:~a~%" (subid-entry-name entry)
                                   (subid-entry-init-id entry)
                                   (subid-entry-last-id entry)) contents))
          ""
          (map selector (subid-configuration-accounts config))))
  (let ((subuid-account-contents (subid-contents subid-account-subuid))
        (subgid-account-contents (subid-contents subid-account-subgid)))
    `(("subuid" ,(plain-file "subuid" subuid-account-contents))
      ("subgid" ,(plain-file "subgid" subgid-account-contents)))))

(define subid-service-type
  (service-type
   (name 'subid)
   (extensions (list (service-extension etc-service-type /etc-subid-entries)))
   (description "Service to configure subuid and subgid")))
