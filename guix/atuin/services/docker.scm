(define-module (atuin services docker)
  #:use-module ((gnu services docker)
                #:prefix guix:)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (gnu packages guile)
  #:use-module (guix gexp)
  #:use-module (json)
  #:use-module (atuin utils)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 hash-table)
  #:use-module (ice-9 match)
  #:export (%empty-daemon-configuration
            docker-service))

;;; Default docker daemon configuration
(define %empty-daemon-configuration
  '((userland-proxy . #t)))

;;; (gnu services docker) does not expose the accessor for the record
;;; <docker-configuration> so we need to recreate them here
(define (docker-configuration-get config field)
  "Get value for field in config."
  ((record-accessor (record-type-descriptor config) field) config))

;;; List of fields that we need to configure in both places:
;;; ((field-in-guix-docker-config field-in-daemon-config) . xs)
(define %docker-config-fields
  `((enable-proxy? userland-proxy)
    (enable-iptables? iptables)
    (debug? debug)))

(define (docker-config-fields-merge daemon-config docker-config)
  "Merge config fields from both configurations. Return a new daemon-config
with all teh values merged."
  (let ((new-daemon-config (alist-copy daemon-config)))
    (let loop ((xs %docker-config-fields))
      (match xs
        ('() new-daemon-config)
        (((docker-config-field daemon-config-field) . xs)
         (let ((in-docker-cfg? (docker-configuration-get docker-config
                                                         docker-config-field))
               (in-daemon-cfg? (assoc-ref daemon-config daemon-config-field)))
           (when (or in-docker-cfg? in-daemon-cfg?)
             (when (not in-daemon-cfg?)
               (set! new-daemon-config (assoc-set! new-daemon-config daemon-config-field #t))))
           (loop xs)))))))

(define* (docker-daemon-config-file daemon-config docker-config)
  "Compute docker daemon.json config file."
  (define proxy #~,(string-append #$(docker-configuration-get docker-config 'proxy) "/bin/proxy"))
  (define build
    (with-extensions (list guile-json-4)
     #~(begin
         (use-modules (srfi srfi-1)
                      (ice-9 match)
                      (json))
         (define (docker-config-values-set! daemon-config daemon-config-values)
           "Set config field values in daemon-config."
           (let ((new-daemon-config (alist-copy daemon-config)))
             (let loop ((xs daemon-config-values))
               (match xs
                 ('() new-daemon-config)
                 (((field ((new-field new-value)) . as) . xs)
                  ;;; TODO: We should iterate over as. Right now only the first pair-is
                  ;;; is set
                  (when (assoc-ref daemon-config field)
                    (set! new-daemon-config (assoc-set! new-daemon-config new-field new-value)))
                  (loop xs))))))
         (call-with-output-file #$output
           (lambda (port)
             (let* ((docker-values `((userland-proxy ((userland-proxy-path #$proxy)))
                                     (debug ((log-level "debug")))))
                    (merged-config `#$(docker-config-fields-merge daemon-config docker-config))
                    (resolved-config (docker-config-values-set! merged-config docker-values)))
               (display (scm->json-string resolved-config #:pretty #t) port)))))))
  (computed-file "dockerd-daemon.json" build))

(define (modify-docker-shepherd-service docker-config daemon-config)
  "Modify the start slot of the docker-shepherd-service. New start method
configures the docker daemon using daemon.json config file."
  (let* ((docker (docker-configuration-get docker-config 'docker))
         (environment-variables
          (docker-configuration-get docker-config 'environment-variables)))
    (lambda (service)
      (shepherd-service
       (inherit service)
       (start #~(make-forkexec-constructor
                 `(,(string-append #$docker "/bin/dockerd")
                   "-p" "/var/run/docker.pid"
                   "--config-file" #$(docker-daemon-config-file daemon-config docker-config))
                 #:environment-variables
                 (list #$@environment-variables)
                 #:pid-file "/var/run/docker.pid"
                 #:log-file "/var/log/docker.log"))))))

(define (modify-docker-service-type-extensions daemon-config extensions)
  "Modify shepherd-root-service-type extension in docker"
  (define (docker-shepherd-service? service)
    (eq? (shepherd-service-canonical-name service) 'dockerd))

  (define (shepherd-root-service-type? extension)
    (eq? (service-type-name (service-extension-target extension)) 'shepherd-root))

  (define (service-type-compute previous-compute)
    "Returns the compute field of an extension replacing docker-shepherd-service 
instance."
    (lambda (config)
      (let ((services (previous-compute config)))
	    (replace services docker-shepherd-service?
			     (modify-docker-shepherd-service config daemon-config)))))

  (replace extensions
           shepherd-root-service-type?
           (lambda (extension)
             (service-extension shepherd-root-service-type
                                (service-type-compute
                                 (service-extension-compute extension))))))

(define* (docker-service #:key (daemon-configuration %empty-daemon-configuration)
			             (docker-configuration (guix:docker-configuration)))
  "Custom docker-service that uses a json config file (daemon.json). It merges
values from docker-configuration (like the original docker-service defined by
guix and daemon-configuration (alist that contains the json configuration for
daemon.json)."
  (let ((docker-service-type
	     (service-type
	      (inherit guix:docker-service-type)
	      (extensions
	       ;; replace 'shepherd-root extension
	       (modify-docker-service-type-extensions
	        daemon-configuration
	        (service-type-extensions guix:docker-service-type)))))
	    (config docker-configuration))
    (service docker-service-type config)))
