(define-module (atuin services nix)
  #:use-module (gnu services)
  #:use-module (gnu system shadow)
  #:use-module (gnu services shepherd)
  #:use-module ((gnu services nix)
                #:prefix guix:)
  #:use-module (guix gexp)
  #:use-module (ice-9 match)
  #:export (nix-service-type))

(define (nix-shepherd-service nix-config)
  (let ((services ((@@ (gnu services nix) nix-shepherd-service) nix-config)))
    (list (shepherd-service
           (inherit (car services))
           (requirement '(user-processes))))))

(define nix-service-type
  (service-type
   (name 'nix)
   (extensions
    (list (service-extension shepherd-root-service-type nix-shepherd-service)
          (service-extension account-service-type
                             (@@ (gnu services nix) nix-accounts))
          (service-extension activation-service-type
                             (@@ (gnu services nix) nix-activation))
          (service-extension etc-service-type
                             (@@ (gnu services nix) nix-service-etc))
          (service-extension profile-service-type
                             (compose list (@@ (gnu services nix) nix-configuration-package)))))
   (description "Run the Nix daemon.")
   (default-value (guix:nix-configuration))))
