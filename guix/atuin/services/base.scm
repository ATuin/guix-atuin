(define-module (atuin services base)
  #:use-module (guix records)
  #:use-module (gnu services shepherd)
  #:use-module (gnu services base)
  #:use-module (gnu packages logging)
  #:use-module (guix gexp)
  #:export (syslog-configuration
            syslog-service-type))

(define-record-type* <syslog-configuration>
  syslog-configuration  make-syslog-configuration
  syslog-configuration?
  (syslogd syslog-configuration-syslogd
           (default (file-append rsyslog "/sbin/rsyslogd")))
  (config-file-arg syslog-configuration-config-file-arg
                   (default "-f"))
  (config-file syslog-configuration-config-file
               (default %default-syslog.conf))
  (extra-args syslog-configuration-extra-args
              (default #f)))

(define syslog-service-type
  (shepherd-service-type
   'syslog
   (lambda (config)
     (shepherd-service
      (documentation "Run the syslog daemon (rsyslogd).")
      (provision '(syslogd))
      (requirement '(user-processes))
      (start #~(let* ((cmd (list #$(syslog-configuration-syslogd config)
                                 #$(syslog-configuration-config-file-arg config)
                                 #$(syslog-configuration-config-file config)))
                      (extra-args #$(syslog-configuration-extra-args config))
                      (spawn (make-forkexec-constructor
                              (if extra-args `(,@cmd ,extra-args) cmd)
                              #:pid-file "/var/run/rsyslogd.pid")))
                 (lambda ()
                   ;; Set the umask such that file permissions are #o640.
                   (let ((mask (umask #o137))
                         (pid  (spawn)))
                     (umask mask)
                     pid))))
      (stop #~(make-kill-destructor))))
   (description "Run the syslog daemon, @command{rsyslogd}, which is
responsible for logging system messages.")))
