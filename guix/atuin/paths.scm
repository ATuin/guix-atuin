(define-module (atuin paths)
  #:use-module (srfi srfi-1)
  #:use-module ((srfi srfi-34)
                #:select (raise))
  #:use-module (ice-9 match)
  #:use-module (guix diagnostics)
  #:use-module (guix i18n)
  #:use-module (guix gexp)
  #:export (./
            %home
            %use-config
            %distro-root-directory
            %channel/distro-root-directory
            make-search-path
            search-file
            search-files
            search-files/errors
            search-files/ensure
            local-files))

(define ./ file-name-separator-string)

(define (%home)
  "Return home for the user running the process or the user specified in
GUIX_ATUIN_USER."
  (passwd:dir (getpwuid (or (getenv "GUIX_ATUIN_USER")
                            (getuid)))))

(define-syntax %use-config
 (syntax-rules ()
      [(_ (m ...))
       (begin
        (add-to-load-path (or (getenv "GUIX_ATUIN_CONFIG")
                              (string-append (%home) "/.guix-config/guix")))
        (use-modules (atuin config m ...)))]))

;; Returns the distro root directory based on the patterns defined
;; in ((FILE THINGS ...) ...)
(define-syntax %distro-root-directory
  (syntax-rules ::: ()
                [(_ (file things :::) rest :::)
                 (letrec-syntax
                     ([dirname* (syntax-rules ()
                                  [(_ file2)
                                   (dirname file2)]
                                  [(_ file2 head tail ...)
                                   (dirname (dirname* file2 tail ...))])]
                      [try (syntax-rules ()
                             [(_ (file* things* ...) rest* ...)
                              (begin
                                (match (search-path %load-path file*)
                                  [#f (try rest* ...)]
                                  [absolute
                                   (dirname* absolute things* ...)]))]
                             [(_) #f])])
                   (try (file things :::)))]))

(define %channel/distro-root-directory
  (%distro-root-directory ("atuin/packages.scm" atuin)))

(define* (make-search-path suffix #:optional root-directory)
  (make-parameter
   (map (lambda (directory)
          (if (string=? directory (or root-directory %channel/distro-root-directory))
              (string-append directory suffix)
              directory))
        %load-path)))

(define* (search-file file-name #:key path (raise-on-error #t))
  "Search SEARCH-PATH for FILE-NAME. Raise an error if file is not found.
if RAISE-ON-ERROR is #f the error is returned instead of being raised."
  (or (search-path (path) file-name)
      (let ([err (formatted-message (G_ "~a: file not found") file-name)])
        (if raise-on-error (raise err) err))))

(define (error-search-file errors)
  (formatted-message
   (G_ "~a: files not found")
   (string-join (map (lambda (e)
                       (car (formatted-message-arguments e))) errors)
                ", ")))

(define-syntax-rule (search-files/errors %search-path file-name ...)
  "Return the list of absolute file names corresponding to each
FILE-NAME found in %SEARCH-PATH."
  (let ([fs (list (search-file file-name #:path %search-path
                               #:raise-on-error #f) ...)])
    (partition (lambda (f) (not (exception? f))) fs)))

;; Return the list of absolute file names corresponding to each
;; FILE-NAME found in %SEARCH-PATH. Files not found are ignored.
(define-syntax search-files
  (syntax-rules ()
    ([_ %search-path file-name ...]
     (call-with-values (lambda () (search-files/errors %search-path file-name ...))
       (lambda (files _) files)))))

;; Return the list of absolute file names corresponding to each
;; FILE-NAME found in %SEARCH-PATH. If any of the files can't be found
;; an error is raised.
(define-syntax search-files/ensure
  (syntax-rules ()
    ([_ %search-path file-name ...]
     (call-with-values (lambda () (search-files/errors %search-path file-name ...))
       (lambda (files errors)
         (if (pair? errors) (raise (error-search-file errors)) files))))))

(define-syntax-rule (local-files %search-path file-name ...)
  "Return the list of local-file derivations corresponding to each
FILE-NAME found in %SEARCH-PATH."
  (let* ([fs (search-files/ensure %search-path file-name ...)])
    (let loop ([xs fs] [ys '()])
      (if (null? xs) (reverse ys)
          (loop (cdr xs) (cons (local-file (car xs)) ys))))))
