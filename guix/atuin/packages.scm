(define-module (atuin packages)
  #:use-module (atuin paths)
  #:use-module (srfi srfi-34)
  #:use-module ((gnu packages)
                #:select (search-patch %patch-path)
                #:prefix guix:)
  #:use-module ((guix packages)
                #:select (package-version)
                #:prefix guix:)
  #:use-module ((guix diagnostics)
                #:select (formatted-message))
  #:use-module ((guix i18n)
                #:select (G_))
  #:export (%patch-path
            search-patch
            search-patches
            search-package-patch
            search-package-patches))

(define %patch-path (make-search-path "/atuin/packages/patches"))

(define (search-patch file-name)
  "Search the patch (for atuin channels) FILE-NAME. Raise an
error if not found."
  (search-file file-name #:path %patch-path))

(define-syntax-rule (search-patches file-name ...)
  "Return the list of absolute file names corresponding to each
FILE-NAME found in %PATCH-PATH."
 (search-files %patch-path file-name ...))

(define (search-package-patch package patch-name)
  "Search the patch with name PATCH-NAME and same version as PACKAGE.
Raise an error if not found."
  (search-patch (string-append patch-name "-"
                               (guix:package-version package) ".diff")))

(define-syntax-rule (search-package-patches package file-name ...)
  "Return the list of absolute file names corresponding to each
FILE-NAME found in %PATCH-PATH."
  (list (search-package-patch package file-name) ...))
