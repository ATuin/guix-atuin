(define-module (atuin utils)
  #:use-module ((guix packages)
                #:select (package-propagated-inputs))
  #:use-module (srfi srfi-1)
  #:export (replace
            package->dependencies))

(define* (get-value-from-env envs #:optional suffix)
  (any (lambda (e) (getenv (string-upcase e))) envs))

(define (replace xs pred proc)
  "Replaces in xs items that satisfy pred with the value produce by proc.
proc is a function that takes as input the item to replace."
  (let loop ((xs xs) (ys '()))
    (cond
     ((null? xs) (reverse ys))
     ((pred (car xs))
      (loop (cdr xs) (cons (proc (car xs)) ys)))
     (else (loop (cdr xs) (cons (car xs) ys))))))

(define (package->dependencies pkg)
  "Return the list of dependencies (as packages) for package pkg"
  (let ((ds (package-propagated-inputs pkg)))
    (map cadr ds)))
